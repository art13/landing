# Privacy Policy 
## Who we are

The '''savecodeshare.eu''' web site is run by the FSFE e.V. in cooperation with OFE Ltd. For information about FSFE, see <https://fsfe.org/about/legal/imprint.html>. For informatoin about OFE, see <http://www.openforumeurope.org/legal-information/>

When this policy refers to "we", this means FSFE and OFE jointly.


## What we collect and why
When you use the website to contact a member of the European Parliament or sign our open letter, we collect and share personal data which you have provided, with your consent. We do this so that we know who has been contacted, how many times, from what countries, and so that the members of the European Parliament, as well as the recipients of the open letter, know who have signed it, where they are from, and how to contact them.

When you contact a member of the European Parliament using our website, we collect and store information about your name, e-mail address and country. This information is also provided by email to the member of the European Parliament you've chosen to contact. We store this information in a database for the duration of the Article 13 campaign and remove it afterwards.

When you sign our open letter, we collect and store information about your name, e-mail address and country, and we will publicly show your name as a signatory to the open letter. We will also provide your name and country to the recipients of the open letter (members of the European Parliament). We will not share your e-mail address. We store this information in a database for the duration of the Article 13 campaign and remove it afterwards.

When you submit the form to join the campaign as an organisation, we collect and store information about your name, organisation, e-mail, country and phone number (if you provide one). This information is stored in our issue system. The information is kept in that system for 30 days after a the issue has been resolved (which in this case means we've reached out to you and had a dialogue about joining the campaign).

## Who has access to stored data
All information submitted through the website, whether stored on the webserver or in our issue system, is available to FSFE and OFE staff, interns and contractors. The information is also available to FSFE's system administrator team. Information is stored unencrypted on our servers, which means the information is also available to staff of PlusServer GmbH where our servers are hosted.

## What your rights are
You have the right to receive the personal data we have stored about you, as well as the right to instruct us to rectify it if it's incorrect. You also have the right to object to our storing of information, in which case we will remove personal data about you, and the right to remove your consent to processing of your personal data in line with this policy, in which case we will also take steps to remove personal data about you.

## Who to contact
If you have questions about our use of your data, would like to request a copy of all information we have stored about you, or would like to talk to our Data Protection Officer for any other purpose, our appointed Data Protection Officer is the FSFE's executive director, whom you can contact at <executive-director@fsfe.org>.

