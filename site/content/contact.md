+++
title = "Contact"
+++

You can reach out to us by filling in the form below, or contact our
team directly:

 * Polina Malaja (FSFE), <polina@fsfe.org>
 * Diana Cocoru (OFE), <diana@openforumeurope.org>

<br />
<br />
<br />
<form method="post" action="https://forms.fsfe.org/email">
   <input type="hidden" name="appid" value="art13contact" />
   <input type="email" name="from" size="35" id="from" placeholder="Your email address" /><br />
   <input type="text" size="35" name="subject" id="subject" placeholder="Subject" /><br />
   <textarea name="content" id="content" rows="5" cols="45" placeholder="Write your message here">
   </textarea><br />
   <input type="submit" value="Send" /><br />
</form>

